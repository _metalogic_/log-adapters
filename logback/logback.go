package main

import (
	"bitbucket.org/_metalogic_/log"
	"bitbucket.org/_metalogic_/logs"
)

// Logger is the exported symbol used to register this plugin
var Logger logback

type logback struct{}

// Common returns CommonLog converted from record
func (l logback) Common(record logs.Record) (common logs.CommonLog, err error) {
	common.Logger = "logback"
	common.Message = record.Message
	if l, ok := record.Event["level"].(string); ok {
		common.Level = logLevel(string(l))
	}
	return common, err
}

func logLevel(level string) (l logs.LogLevel) {
	switch level {
	case "DEBUG":
		l = logs.Debug
	case "INFO":
		l = logs.Info
	case "WARN":
		l = logs.Warn
	case "ERROR":
		l = logs.Error
	default:
		log.Errorf("invalid log level '%s' in record", level)
	}
	return l
}
