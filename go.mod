module bitbucket.org/_metalogic_/log-adapters

go 1.18

require (
	bitbucket.org/_metalogic_/log v1.5.0
	bitbucket.org/_metalogic_/logs v1.1.0
)

require (
	bitbucket.org/_metalogic_/color v1.0.4 // indirect
	bitbucket.org/_metalogic_/colorable v1.0.3 // indirect
	bitbucket.org/_metalogic_/isatty v1.0.4 // indirect
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a // indirect
	golang.org/x/sys v0.0.0-20220622161953-175b2fd9d664 // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
