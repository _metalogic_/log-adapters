package main

import (
	"net/http"
	"strings"
)

type HaproxyLog struct {
	Type                 string `json:"type"`
	HTTPStatus           int    `json:"http_status"`
	HTTPRequest          string `json:"http_request"`
	RemoteAddress        string `json:"remote_addr"`
	BytesRead            int    `json:"bytes_read"`
	UpstreamAddress      string `json:"upstream_addr"`
	BackendName          string `json:"backend_name"`
	Retries              int    `json:"retries"`
	BytesUploaded        int    `json:"bytes_uploaded"`
	UpstreamResponseTime int    `json:"upstream_response_time"`
	UpstreamConnectTime  int    `json:"upstream_connect_time"`
	SessionDuration      int    `json:"session_duration"`
	TerminationState     string `json:"termination_state"`
}

func (log HaproxyLog) omit() bool {
	return log.HTTPStatus == http.StatusOK && (strings.Contains(log.HTTPRequest, "/passwordreset/v1/health") || strings.Contains(log.HTTPRequest, "/person/v1/health"))
}
