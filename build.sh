#!/bin/sh

go build -buildmode=plugin -o golang/golang.so golang/golang.go
go build -buildmode=plugin -o logback/logback.so logback/logback.go
go build -buildmode=plugin -o mule/mule.so mule/mule.go
go build -buildmode=plugin -o serilog/serilog.so serilog/serilog.go
