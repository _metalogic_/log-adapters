package main

import (
	"bitbucket.org/_metalogic_/log"
	"bitbucket.org/_metalogic_/logs"
)

// Logger is the exported symbol used to register this plugin
var Logger serilog

type serilog struct{}

// Common returns CommonLog converted from record
func (l serilog) Common(record logs.Record) (common logs.CommonLog, err error) {
	common.Logger = "serilog"
	common.Message = record.Message
	if l, ok := record.Event["level"].(string); ok {
		common.Level = logLevel(string(l))
	}

	if f, ok := record.Event["fields"].(map[string]interface{}); ok {
		common.Fields = map[string]interface{}(f)
	}

	if c, ok := common.Fields["SourceContext"].(string); ok {
		common.Context = string(c)
	}

	return common, err
}

func logLevel(level string) (l logs.LogLevel) {
	switch level {
	case "Debug":
		l = logs.Debug
	case "Information":
		l = logs.Info
	case "Warning":
		l = logs.Warn
	case "Error":
		l = logs.Error
	default:
		log.Errorf("invalid log level '%s' in record", level)
		l = logs.Any
	}
	return l
}
