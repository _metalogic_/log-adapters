package main

import (
	"fmt"

	"bitbucket.org/_metalogic_/log"
	"bitbucket.org/_metalogic_/logs"
)

// Logger is the exported symbol used to register this plugin
var Logger golang

type golang struct{}

// Common returns CommonLog converted from record
func (l golang) Common(record logs.Record) (common logs.CommonLog, err error) {
	common.Logger = "golang"
	common.Message = record.Message
	common.Level = logLevel(record)
	var file string
	if f, ok := record.Event["file"].(string); ok {
		file = string(f)
	}
	var line int
	if l, ok := record.Event["line"].(float64); ok {
		line = int(l)
	}
	if file != "" {
		common.Context = fmt.Sprintf("%s (line %d)", file, line)
	}
	common.Fields = record.Event

	return common, err
}

func logLevel(record logs.Record) (l logs.LogLevel) {
	if level, ok := record.Event["level"].(string); ok {
		switch string(level) {
		case "trace":
			l = logs.Trace
		case "debug":
			l = logs.Debug
		case "info":
			l = logs.Info
		case "warn":
			l = logs.Warn
		case "error":
			l = logs.Error
		case "fatal":
			l = logs.Fatal
		default:
			log.Errorf("invalid log level '%s' in record '%+v'", string(level), record)
		}
	} else {
		l = logs.Any
	}
	return l
}
