package main

import (
	"bitbucket.org/_metalogic_/log"
	"bitbucket.org/_metalogic_/logs"
)

// Logger is the exported symbol used to register this plugin
var Logger mule

type mule struct{}

// Common returns CommonLog converted from record
func (l mule) Common(record logs.Record) (common logs.CommonLog, err error) {
	common.Logger = "mule"
	common.Message = record.Message
	common.Level = logLevel(record)
	if component, ok := record.Event["prefix"].(string); ok {
		common.Component = component
	}
	var thread string
	if f, ok := record.Event["threadName"].(string); ok {
		thread = string(f)
	}
	common.Context = thread
	return common, err
}

func logLevel(record logs.Record) (l logs.LogLevel) {
	if level, ok := record.Event["level"].(string); ok {
		switch string(level) {
		case "DEBUG":
			l = logs.Debug
		case "INFO":
			l = logs.Info
		case "WARN":
			l = logs.Warn
		case "ERROR":
			l = logs.Error
		default:
			log.Errorf("invalid log level '%s' in record", string(level))
		}
	} else {
		log.Tracef("mule: failed to convert level in record '%+v'", record)
		l = logs.Trace
	}
	return l
}
